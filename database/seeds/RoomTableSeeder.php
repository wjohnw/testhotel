<?php

use Illuminate\Database\Seeder;

class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hotels = App\Models\Hotel::pluck('id')->toArray();
        $clients = App\Models\Client::pluck('id')->toArray();

        factory(App\Models\Room::class, 10)->create(
            [
                'hotel_id' => array_rand($hotels),
                'client_id' => array_rand($clients)
            ]
        );
    }
}
