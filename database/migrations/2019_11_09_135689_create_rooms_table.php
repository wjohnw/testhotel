<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {

            Schema::disableForeignKeyConstraints();

            $table->bigIncrements('id');
            $table->unsignedInteger('number');
            $table->boolean('booked');
            $table->integer('hotel_id');
            $table->integer('client_id');
            $table->foreign('hotel_id')->references('id')->on('hotels');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->timestamps();

            Schema::enableForeignKeyConstraints();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
