<?php

namespace App\Interfaces;

use Illuminate\Support\Collection;

interface ReservationInterface {
    //TODO which return typ in Collection or Reservation
    public function create(array $data): void;
    public function getBookedRoomsAndClientsByHotelId(int $id): Collection;
}
