<?php

namespace App\Interfaces;

use App\Models\Hotel;

interface HotelInterface {

    public function getById(int $id): Hotel;
}
