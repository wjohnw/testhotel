<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Hotel;
use App\Models\Room;

use App\Services\HotelService;
use App\Services\ReservationService;

use Carbon\Carbon;

class HotelsController extends Controller
{
    private $hotelService;
    private $reservationService;

    public function __construct(
        HotelService $hotelService,
        ReservationService $reservationService
    )
    {
        $this->hotelService = $hotelService;
        $this->reservationService = $reservationService;
    }

    public function show($id)
    {
        $randomClientId = rand(1,  Client::all()->count());
        $randomRoomId = rand(1, Room::all()->count());
        $randomHotelId = rand(1, Hotel::all()->count());
        $startDate = Carbon::now();
        $endDate = Carbon::now()->addDays(3);

        $this->reservationService->createReservation(
            $randomHotelId,
            $randomRoomId,
            $randomClientId,
            $startDate,
            $endDate
        );

        $hotelReservationData = $this->reservationService->getBookedRoomsAndClientsByHotelId($id);
        //TODO formatted data with API resources
        return response()->json($hotelReservationData);

    }
}
