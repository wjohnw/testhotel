<?php

namespace App\Services;

use App\Interfaces\ReservationInterface;

class ReservationService {

    private $reservationRepository;

    public function __construct(ReservationInterface $reservationRepository)
    {
        $this->reservationRepository = $reservationRepository;
    }

    public function createReservation($hotelId, $roomId, $clientId, $startDate, $endDate )
    {
        $this->reservationRepository->create(
            [
                'hotel_id' => $hotelId,
                'room_id' => $roomId,
                'client_id' => $clientId,
                'start_date' => $startDate,
                'end_date' => $endDate
             ]
        );
    }

    public function getBookedRoomsAndClientsByHotelId($id)
    {
        return $this->reservationRepository->getBookedRoomsAndClientsByHotelId($id);
    }

}
