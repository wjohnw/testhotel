<?php

namespace App\Services;

use App\Interfaces\HotelInterface;

class HotelService {

    private $hotelRepository;

    public function __construct(HotelInterface $hotelRepository)
    {
        $this->hotelRepository = $hotelRepository;
    }

    public function getById($id)
    {
        $hotelWithRooms = $this->hotelRepository->getById($id);

        return $hotelWithRooms;
    }
}
