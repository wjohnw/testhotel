<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    public $timestamps = true;

    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }

}
