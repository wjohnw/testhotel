<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';
    public $timestamps = true;

    public function hotel()
    {
        return $this->belongTo(Hotel::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

}
