<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = 'hotels';
    public $timestamps = true;

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

}
