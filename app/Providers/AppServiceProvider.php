<?php

namespace App\Providers;

use App\Interfaces\HotelInterface;
use App\Interfaces\ReservationInterface;
use App\Repositories\HotelRepository;
use App\Repositories\ReservationRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ReservationInterface::class, ReservationRepository::class);
        $this->app->bind(HotelInterface::class, HotelRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
