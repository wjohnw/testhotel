<?php

namespace App\Repositories;

use App\Interfaces\ReservationInterface;
use App\Models\Reservation;
use Illuminate\Support\Collection;

class ReservationRepository implements ReservationInterface{

    public function create(array $data): void
    {
        Reservation::create($data);
    }

    public function getBookedRoomsAndClientsByHotelId(int $id): Collection
    {
        return Reservation::with('room')->with('client')->where('hotel_id', $id)->get();
    }
}
