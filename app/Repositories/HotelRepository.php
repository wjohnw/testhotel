<?php

namespace App\Repositories;

use App\Interfaces\HotelInterface;
use App\Models\Hotel;

class HotelRepository implements HotelInterface{

    public function getById(int $id): Hotel
    {
        return Hotel::find($id);
    }
}
